Assumptions:

- FeeBreakpoints is not a big list of data and performance is not concern.
If performance actually is concern - we can approach this issue 
by caching or by using one of sorting algorithms (eg. binary search) instead of simple foreach loop

- FeeBreakpoints will follow same MIN and MAX rules as defined in task (i created some basic validation just in case)

- FeeBreakpointRepository will return sorted breakpoints, no sorting is needed on application layer

- There is no "duplicate" FeeBreakpoint amounts (eg. 1000 - 90 and 1000 - 120)


Notes:

- FeeBreakpointRepository is stupid simple implementation mimicking response we would get if using ORM.

- I made several assumptions regarding business rules, which are documented throughout code
