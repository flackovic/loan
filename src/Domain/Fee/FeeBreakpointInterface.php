<?php

declare(strict_types=1);

namespace App\Domain\Fee;

interface FeeBreakpointInterface
{
    public function getAmount(): float;

    public function getFee(): float;
}
