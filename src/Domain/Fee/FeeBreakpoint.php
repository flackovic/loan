<?php

declare(strict_types=1);

namespace App\Domain\Fee;

class FeeBreakpoint implements FeeBreakpointInterface
{
    /** @var int */
    private $amount;
    /** @var int */
    private $fee;

    public function __construct(int $amount, int $fee)
    {
        $this->amount = $amount;
        $this->fee = $fee;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getFee(): float
    {
        return $this->fee;
    }
}
