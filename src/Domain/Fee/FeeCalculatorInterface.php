<?php

declare(strict_types=1);

namespace App\Domain\Fee;

use App\Domain\Loan\LoanApplication;

/**
 * Calculates fees for loan applications.
 */
interface FeeCalculatorInterface
{
    /**
     * Calculates the fee for a loan application.
     *
     * @param LoanApplication $application the loan application to
     *                                     calculate for
     *
     * @return float the calculated fee
     */
    public function calculate(LoanApplication $application): float;
}
