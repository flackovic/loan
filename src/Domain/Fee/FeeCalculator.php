<?php

declare(strict_types=1);

namespace App\Domain\Fee;

use App\Domain\Loan\LoanApplication;

class FeeCalculator implements FeeCalculatorInterface
{
    /** @var FeeBreakpointRepositoryInterface */
    private $feeBreakpointRepository;

    public function __construct(FeeBreakpointRepositoryInterface $feeBreakpointRepository)
    {
        $this->feeBreakpointRepository = $feeBreakpointRepository;
    }

    /**
     * Calculates the fee for a loan application.
     *
     * @param LoanApplication $application the loan application to
     *                                     calculate for
     *
     * @return float the calculated fee
     */
    public function calculate(LoanApplication $application): float
    {
        // @todo assumption - fee breakpoints are sorted by amount
        $feeBreakpoints = $this->feeBreakpointRepository->getFeeBreakpointsForTerm($application->getTerm());

        foreach ($feeBreakpoints as $order => $feeBreakpoint) {
            if (true === $this->applicationAmountIsSameValueAsFeeBreakpoint($application, $feeBreakpoint)) {
                return $feeBreakpoint->getFee();
            }

            $nextFeeBreakPoint = $feeBreakpoints[$order + 1] ?? null;

            // @todo Business rule assumption: If there is no higher breakpoint - we just return lower one
            if (true === $this->lowerBreakpointIsFoundButThereIsNoHigherBreakpoint($application, $feeBreakpoint, $nextFeeBreakPoint)) {
                return $feeBreakpoint->getFee();
            }

            /** @var FeeBreakpoint $nextFeeBreakPoint */
            if (true === $this->lowerBreakpointAndHigherBreakpointAreFound($application, $feeBreakpoint, $nextFeeBreakPoint)) {
                $diff = ($application->getAmount() - $feeBreakpoint->getAmount()) / ($nextFeeBreakPoint->getAmount() - $feeBreakpoint->getAmount());
                $fee = $feeBreakpoint->getFee() + (($nextFeeBreakPoint->getFee() - $feeBreakpoint->getFee()) * $diff);

                return (float) $fee;
            }
        }

        // This should not happen as we have MIN and MAX constraints on LoanApplication, but still - we need to guard here in case
        // FeeBreakpoints, for whatever reason, do not follow same rule.
        // @todo Business rule assumption: If Loan amount is lower than any of provided FeeBreakpoints, we return lowest one from the list
        return $feeBreakpoints[0]->getFee();
    }

    private function applicationAmountIsSameValueAsFeeBreakpoint(LoanApplication $application, FeeBreakpoint $feeBreakpoint): bool
    {
        return $feeBreakpoint->getAmount() === $application->getAmount();
    }

    private function lowerBreakpointIsFoundButThereIsNoHigherBreakpoint(LoanApplication $application, FeeBreakpoint $feeBreakpoint, ?FeeBreakpoint $nextFeeBreakpoint): bool
    {
        return $application->getAmount() > $feeBreakpoint->getAmount() && null === $nextFeeBreakpoint;
    }

    private function lowerBreakpointAndHigherBreakpointAreFound(LoanApplication $application, FeeBreakpoint $feeBreakpoint, FeeBreakpoint $nextFeeBreakpoint): bool
    {
        return $application->getAmount() > $feeBreakpoint->getAmount() && $application->getAmount() < $nextFeeBreakpoint->getAmount();
    }
}
