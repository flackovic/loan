<?php

declare(strict_types=1);

namespace App\Domain\Fee;

interface FeeBreakpointRepositoryInterface
{
    /** @return FeeBreakpoint[] */
    public function getFeeBreakpointsForTerm(int $term): array;
}
