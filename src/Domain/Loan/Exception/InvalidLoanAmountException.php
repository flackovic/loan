<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: latz
 * Date: 14/01/20
 * Time: 23:51
 */

namespace App\Domain\Loan\Exception;

class InvalidLoanAmountException extends \Exception
{
}
