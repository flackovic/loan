<?php

declare(strict_types=1);

namespace App\Domain\Loan\Exception;

class InvalidLoanTermException extends \Exception
{
}
