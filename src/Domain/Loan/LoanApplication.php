<?php

declare(strict_types=1);

namespace App\Domain\Loan;

use App\Domain\Loan\Exception\InvalidLoanAmountException;
use App\Domain\Loan\Exception\InvalidLoanTermException;

/**
 * A cut down version of a loan application containing
 * only the required properties for this test.
 */
class LoanApplication
{
    private const MINIMUM_LOAN_AMOUNT = 1000;
    private const MAXIMUM_LOAN_AMOUNT = 20000;

    private const VALID_TERMS = [12, 24];

    /**
     * @var int
     */
    private $term;

    /**
     * @var float
     */
    private $amount;

    public function __construct(int $term, float $amount)
    {
        if (false === \in_array($term, self::VALID_TERMS, true)) {
            throw new InvalidLoanTermException(sprintf('Loan term must be a valid value. (%s)', implode(', ', self::VALID_TERMS)));
        }

        if ($amount < self::MINIMUM_LOAN_AMOUNT || $amount > self::MAXIMUM_LOAN_AMOUNT) {
            throw new InvalidLoanAmountException(sprintf('Loan amount must be between %d and %d', self::MINIMUM_LOAN_AMOUNT, self::MAXIMUM_LOAN_AMOUNT));
        }

        $this->term = $term;
        $this->amount = $amount;
    }

    /**
     * Gets the term for this loan application expressed
     * in number of months.
     */
    public function getTerm(): int
    {
        return $this->term;
    }

    /**
     * Gets the amount requested for this loan application.
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}
