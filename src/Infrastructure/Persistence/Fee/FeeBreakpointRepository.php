<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Fee;

use App\Domain\Fee\FeeBreakpoint;
use App\Domain\Fee\FeeBreakpointRepositoryInterface;
use RuntimeException;

/**
 * Note: This repository is stupid-simple implementation just to provide some data.
 * I assume this would be stored in DB.
 */
class FeeBreakpointRepository implements FeeBreakpointRepositoryInterface
{
    /**
     * @return FeeBreakpoint[]
     */
    public function getFeeBreakpointsForTerm(int $term): array
    {
        if (12 === $term) {
            return $this->provideBreakpointsForTerm12();
        }

        if (24 === $term) {
            return $this->provideBreakpointsForTerm24();
        }

        // @todo throw domain exception
        throw new RuntimeException('Term is not supported. Implement domain exception');
    }

    private function provideBreakpointsForTerm12(): array
    {
        $rawBreakpoints = [
            1000 => 50,
            2000 => 90,
            3000 => 90,
            4000 => 115,
            5000 => 100,
            6000 => 120,
            7000 => 140,
            8000 => 160,
            9000 => 180,
            10000 => 200,
            11000 => 220,
            12000 => 240,
            13000 => 260,
            14000 => 280,
            15000 => 300,
            16000 => 320,
            17000 => 340,
            18000 => 360,
            19000 => 380,
            20000 => 400,
        ];

        $breakpoints = [];
        foreach ($rawBreakpoints as $breakpointAmount => $breakpointFee) {
            $breakpoints[] = new FeeBreakpoint($breakpointAmount, $breakpointFee);
        }

        return $breakpoints;
    }

    private function provideBreakpointsForTerm24(): array
    {
        $rawBreakpoints = [
            1000 => 70,
            2000 => 100,
            3000 => 120,
            4000 => 160,
            5000 => 200,
            6000 => 240,
            7000 => 280,
            8000 => 320,
            9000 => 360,
            10000 => 400,
            11000 => 440,
            12000 => 480,
            13000 => 520,
            14000 => 560,
            15000 => 600,
            16000 => 640,
            17000 => 680,
            18000 => 720,
            19000 => 760,
            20000 => 800,
        ];

        $breakpoints = [];
        foreach ($rawBreakpoints as $breakpointAmount => $breakpointFee) {
            $breakpoints[] = new FeeBreakpoint($breakpointAmount, $breakpointFee);
        }

        return $breakpoints;
    }
}
