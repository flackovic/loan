<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Fee;

use App\Domain\Fee\FeeBreakpoint;
use App\Domain\Fee\FeeBreakpointRepositoryInterface;
use App\Domain\Fee\FeeCalculator;
use App\Domain\Loan\LoanApplication;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class FeeCalculatorTest extends TestCase
{
    /** @var FeeCalculator */
    private $feeCalculator;

    public function setUp(): void
    {
        /** @var FeeBreakpointRepositoryInterface|MockObject $feeRepositoryMock */
        $feeRepositoryMock = $this->createMock(FeeBreakpointRepositoryInterface::class);
        $feeRepositoryMock->method('getFeeBreakpointsForTerm')
            ->with(24)
            ->willReturn([
                new FeeBreakpoint(1000, 70),
                new FeeBreakpoint(2000, 100),
                new FeeBreakpoint(3000, 120),
            ]);

        $this->feeCalculator = new FeeCalculator($feeRepositoryMock);
    }

    public function testCalculateWillReturnFloat(): void
    {
        $result = $this->feeCalculator->calculate(new LoanApplication(24, 1000));

        $this->assertTrue(\is_float($result));
    }

    /** @dataProvider provideLoanData */
    public function testCalculateWillReturnCorrectValue($term, $amount, $expectedResult): void
    {
        $result = $this->feeCalculator->calculate(new LoanApplication($term, $amount));

        $this->assertSame($expectedResult, $result);
    }

    public function provideLoanData(): iterable
    {
        yield 'Amount is same as first fee breakpoint on the list' => [
            24,
            1000,
            70.0,
        ];

        yield 'Amount is same as last fee breakpoint on the list' => [
            24,
            3000,
            120.0,
        ];

        yield 'Amount is between two breakpoints' => [
            24,
            2500,
            110.0,
        ];

        yield 'Amount does not have higher fee breakpoint' => [
            24,
            3001,
            120.0,
        ];

        yield 'Amount is lower than any of provided breakpoints' => [
            24,
            3001,
            120.0,
        ];
    }
}
