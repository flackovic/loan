<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Loan;

use App\Domain\Loan\Exception\InvalidLoanAmountException;
use App\Domain\Loan\Exception\InvalidLoanTermException;
use App\Domain\Loan\LoanApplication;
use PHPUnit\Framework\TestCase;

class LoanApplicationTest extends TestCase
{
    public function testItCanBeConstructedGivenValidDataIsProvided(): void
    {
        $this->assertInstanceOf(LoanApplication::class, new LoanApplication(24, 2000));
    }

    public function testItWillThrowInvalidLoanTermExceptionGivenTermIsNotOneOfTheValidValues(): void
    {
        $this->expectException(InvalidLoanTermException::class);
        $this->expectExceptionMessage('Loan term must be a valid value. (12, 24)');

        new LoanApplication(1, 2500);
    }

    public function testItWillThrowInvalidLoanAmountExceptionGivenAmountIsLessThenMinimal(): void
    {
        $this->expectException(InvalidLoanAmountException::class);
        $this->expectExceptionMessage('Loan amount must be between 1000 and 20000');

        new LoanApplication(24, 1);
    }

    public function testItWillThrowInvalidLoanAmountExceptionGivenAmountIsHigherThenMaximum(): void
    {
        $this->expectException(InvalidLoanAmountException::class);
        $this->expectExceptionMessage('Loan amount must be between 1000 and 20000');

        new LoanApplication(24, 30000);
    }
}
